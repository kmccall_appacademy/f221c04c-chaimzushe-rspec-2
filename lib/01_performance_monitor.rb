def measure(amount=1)
   start = Time.now
   amount.times {yield}
   (Time.now - start) / amount
end
