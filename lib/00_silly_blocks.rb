def reverser
  yield.split(" ").map do |word|
    word.reverse
  end.join(" ")
end

def adder(n=1)
  yield + n
end

def repeater(amount=1)
  amount.times{|time| yield}
end
